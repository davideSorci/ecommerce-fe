import { Component, inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  user: User = {
    userId: 0,
    email: '',
    password: ''
  };

  authService = inject(AuthService);
  
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  constructor (private router: Router) { }

  login(): void {
    this.user.email = this.loginForm.value.email ?? '';
    this.user.password = this.loginForm.value.password ?? '';

    this.authService.login(this.user)
      .subscribe(user => {
        this.user.userId = user.userId;
        localStorage.setItem('token', user.token ?? '');
        localStorage.setItem('userId', this.user.userId.toString());
        if (localStorage.getItem('token') != '') {
          this.router.navigate(['/']);
        }
        this.user = user;
      });
  }
}
