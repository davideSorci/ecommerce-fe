import { Component, inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { NewUser } from '../../interfaces/newUser';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  newUser: NewUser = {
    name: '',
    surname: '',
    email: '',
    phone: '',
    password: ''
  };

  authService = inject(AuthService);

  registrationForm = new FormGroup({
    name: new FormControl(''),
    surname: new FormControl(''),
    email: new FormControl(''),
    phone: new FormControl(''),
    password: new FormControl('')
  });

  constructor (private router: Router) { }

  registration(): void {
    this.newUser.name = this.registrationForm.value.name ?? '';
    this.newUser.surname = this.registrationForm.value.surname ?? '';
    this.newUser.email = this.registrationForm.value.email ?? '';
    this.newUser.phone = this.registrationForm.value.phone ?? '';
    this.newUser.password = this.registrationForm.value.password ?? '';

    this.authService.registration(this.newUser)
        .subscribe(newUser => {
          localStorage.setItem('token', newUser.token ?? '');
        
          if (localStorage.getItem('token') != '') {
            this.router.navigate(['/']);
          }

          this.newUser = newUser
        });
  }
}
