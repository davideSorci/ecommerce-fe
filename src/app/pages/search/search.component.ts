import { Component } from '@angular/core';
import { ProductAdvertisement } from 'src/app/interfaces/productAdvertisement';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  products: ProductAdvertisement[] = [];

}
