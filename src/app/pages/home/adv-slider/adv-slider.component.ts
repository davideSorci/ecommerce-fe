import { Component } from '@angular/core';

import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-adv-slider',
  templateUrl: './adv-slider.component.html',
  styleUrls: ['./adv-slider.component.css']
})
export class AdvSliderComponent {

  isMobile: number = window.innerWidth;
  currentSlider: number = 0;
  sliderImages: string[] = [
    "./../../../assets/slider-home/fire-tv.png",
    "./../../../assets/slider-home/xiaomi-fire-tv.jpg",
    "./../../../assets/slider-home/vetrina-stellare.jpg",
    "./../../../assets/slider-home/fashion.jpg",
    "./../../../assets/slider-home/gaming-week.jpg"
  ];
  sliderImagesMobile: string[] = [
    "./../../../assets/slider-home/mobile/fire-tv-mobile.png",
    "./../../../assets/slider-home/mobile/account-20-mobile.jfif",
    "./../../../assets/slider-home/mobile/fashion-mobile.jpg",
    "./../../../assets/slider-home/mobile/gaming-week-mobile.jpg",
    "./../../../assets/slider-home/mobile/xiaomi-fire-tv-mobile.jpg"
  ];

  ngOnInit() {
    fromEvent(window, 'resize')
      .subscribe(event => {
        const w = event.target as Window;
        this.isMobile = w.innerWidth;
      });
  }

  increaseSlider() {
    return this.currentSlider < this.sliderImages.length - 1 ?
            this.currentSlider = ++this.currentSlider :
            this.currentSlider = 0;
  }

  decreaseSlider() {
    return this.currentSlider == 0 ?
            this.currentSlider = this.sliderImages.length - 1 :
            this.currentSlider = --this.currentSlider;
  }
}
