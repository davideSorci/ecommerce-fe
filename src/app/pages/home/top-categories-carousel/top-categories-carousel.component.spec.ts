import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopCategoriesCarouselComponent } from './top-categories-carousel.component';

describe('TopCategoriesCarouselComponent', () => {
  let component: TopCategoriesCarouselComponent;
  let fixture: ComponentFixture<TopCategoriesCarouselComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TopCategoriesCarouselComponent]
    });
    fixture = TestBed.createComponent(TopCategoriesCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
