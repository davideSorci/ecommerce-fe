import { Component } from '@angular/core';

import { Categories } from 'src/app/interfaces/categories';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-top-categories-carousel',
  templateUrl: './top-categories-carousel.component.html',
  styleUrls: ['./top-categories-carousel.component.css']
})
export class TopCategoriesCarouselComponent {

  translationX: number = 0;
  singleTranslatation: number = window.innerWidth > 800 ? 130 : 90;
  remainTranslatableWidth: number = 0;
  topCategories: Categories[] = [];
  categories: Categories[] = [];

  constructor(private categoriesService: CategoriesService) {}

  ngOnInit() {
    this.getAllCategories();
    this.remainTranslatableWidth = this.calculateRemainingTranslateX(window.innerWidth);
  }

  onResize() {
    this.remainTranslatableWidth = this.calculateRemainingTranslateX(window.innerWidth);
    if(this.remainTranslatableWidth < -this.singleTranslatation / 2) {
      this.remainTranslatableWidth = 0;
      this.translationX += this.singleTranslatation;
      const slider = document.getElementById("categories-container");
      if (slider != null)
        slider.style.transform = 'translateX(' + this.translationX + 'px)';
    } 
  }

  calculateRemainingTranslateX(windowWidth: number): number {
    let carouselWidth = windowWidth > 800 ? 1480 : 990;
    let bodyWidth = document.getElementById("categories")?.offsetWidth ?? 0;
    return carouselWidth - bodyWidth + this.translationX;
  }

  getAllCategories(): void {
    this.categoriesService.getAllCategories()
      .subscribe(categories => {
        this.categories = categories;
        this.topCategories = this.categories.filter(c => c.icon != '');
        this.topCategories = this.topCategories.sort(
          (num, numToCompare) => numToCompare.rating - num.rating
        );
      });
  }

  slideLeft() {
    const slider = document.getElementById("categories-container");
    if (slider != null) {
      this.translationX = this.translationX + this.singleTranslatation;
      slider.style.transform = 'translateX(' + this.translationX + 'px)';
    }
    this.remainTranslatableWidth = this.calculateRemainingTranslateX(window.innerWidth);
  }

  slideRight() {
    const slider = document.getElementById("categories-container");
    if (slider != null) {
      this.translationX = this.translationX - this.singleTranslatation;
      slider.style.transform = 'translateX(' + this.translationX + 'px)';
    }
    this.remainTranslatableWidth = this.calculateRemainingTranslateX(window.innerWidth);
  }
}
