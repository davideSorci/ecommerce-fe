import { Component } from '@angular/core';
import { Products } from 'src/app/interfaces/products';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.css']
})
export class RecommendedComponent {

  products: Products[] = [];
  topDiscountedProducts: Products[] = [];
  topSoldProducts: Products[] = [];
  baseUrlImg: string = "assets";

  constructor(private productsServices: ProductsService) {}

  ngOnInit() {
    this.getPublishedProducts();
  }

  getPublishedProducts(): void {
    this.productsServices.getPublishedProducts()
      .subscribe(products => {
        this.products = products;
        this.topDiscountedProducts = this.products.sort(
          (product, productToCompare) => productToCompare.discount - product.discount
        ).slice(0, 4);
        this.topSoldProducts = this.products.sort(
          (product, productToCompare) => productToCompare.unitSold - product.unitSold
        ).slice(0, 3);
      })
  }
}
