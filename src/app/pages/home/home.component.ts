import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  isMobile: number = window.innerWidth;
  toggleMenu: boolean = false;
  navCloseMobile: string = "'header' 'body'";
  navOpenMobile: string = "'header' 'nav' 'body'";
  navDesktop: string = "'header header' 'nav body'";

  onResize() {
    this.isMobile = window.innerWidth;
  }
}
