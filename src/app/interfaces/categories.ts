export interface Categories {
    name: string;
    rating: number;
    description: string;
    icon: string;
}