export interface Products {
    price: number,
    description: string,
    discount: number,
    brand: string,
    title: string,
    imgPath: string,
    unitSold: number
}