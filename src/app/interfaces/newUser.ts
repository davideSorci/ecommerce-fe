export interface NewUser {
    name: string;
    surname: string;
    email: string;
    phone: string;
    password: string;
    token?: string;
}