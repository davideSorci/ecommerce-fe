import { Component, Input, Output, EventEmitter, inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

import { AuthService } from 'src/app/services/auth.service';
import { AdvertisementsService } from 'src/app/services/advertisements.service';
import { ProductAdvertisement } from 'src/app/interfaces/productAdvertisement';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  searchForm = new FormGroup({
    keyword: new FormControl('')
  });

  @Input() toggle: boolean = false;
  @Output() toggleChange = new EventEmitter<boolean>();
  @Output() productsToEmit = new EventEmitter<ProductAdvertisement[]>();
  authService = inject(AuthService);
  advService = inject(AdvertisementsService);

  constructor(private router: Router) {}

  toggleMenu(): void {
    this.toggle = !this.toggle;
    this.toggleChange.emit(this.toggle);
  }

  research(): void {
    this.searchProductsWithUserLoggedIn();
    this.router.navigate(['/search']);
  }

  searchProductsWithUserLoggedIn(): void {
    this.advService.searchProductsUserLoggedIn(localStorage.getItem('userId') || '', this.searchForm.value.keyword ?? '')
      .subscribe(products => {
        this.productsToEmit.emit(products);
      });
  }

  logoutUser() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  isUserloggedIn(): boolean {
    if (localStorage.getItem('token') != null) 
      return true;
    else 
      return false;
  }
}
