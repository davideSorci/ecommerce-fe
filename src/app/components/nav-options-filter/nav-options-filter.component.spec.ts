import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavOptionsFilterComponent } from './nav-options-filter.component';

describe('NavOptionsFilterComponent', () => {
  let component: NavOptionsFilterComponent;
  let fixture: ComponentFixture<NavOptionsFilterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NavOptionsFilterComponent]
    });
    fixture = TestBed.createComponent(NavOptionsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
