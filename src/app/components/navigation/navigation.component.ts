import { Component, Input, inject } from '@angular/core';
import { Router } from '@angular/router';

import { Categories } from 'src/app/interfaces/categories';
import { CategoriesService } from 'src/app/services/categories.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  categories: Categories[] = [];
  category: Categories = {
    name: '',
    rating: 0,
    description: '',
    icon: ''
  }
  @Input() toggle: boolean = false;
  authService = inject(AuthService);
  
  constructor(private categoriesService: CategoriesService, private router: Router) { }

  ngOnInit(): void {
    this.getAllCategories();
  }

  getAllCategories(): void {
    this.categoriesService.getAllCategories()
      .subscribe(categories => this.categories = categories);
  }

  logoutUser() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  isUserloggedIn(): boolean {
    if (localStorage.getItem('token') != null) 
      return true;
    else 
      return false;
  }

  updateRating(category: Categories) {
    category.rating += 1;
    this.categoriesService.updateCategoryRating(category)
      .subscribe(category => this.category = category);
  }
}
