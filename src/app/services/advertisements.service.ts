import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ProductAdvertisement } from '../interfaces/productAdvertisement';
import { catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdvertisementsService {

  //private url = 'http://13.53.46.222';
  private url = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  searchProducts(keyword: string): Observable<ProductAdvertisement[]> {
    return this.http.get<ProductAdvertisement[]>(
      this.url.concat("/v1/advertisement/search/" + keyword)).pipe(
        catchError(this.handleError<ProductAdvertisement[]>('getPublishedProducts'))
      );  
  }

  searchProductsUserLoggedIn(userId: string, keyword: string): Observable<ProductAdvertisement[]> {
    return this.http.get<ProductAdvertisement[]>(
      this.url.concat("/v1/advertisement/" + userId + "/search/" + keyword)).pipe(
        catchError(this.handleError<ProductAdvertisement[]>('getPublishedProducts'))
      );  
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
