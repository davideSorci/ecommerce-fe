import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { User } from '../interfaces/user';
import { NewUser } from '../interfaces/newUser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //private url = 'http://13.53.46.222';
  private url = 'http://localhost:8080';
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS' })
  };

  constructor(private http: HttpClient) { }

  login(user: User): Observable<User> {
    return this.http.post<User>(this.url.concat("/v1/authentication/login"), user, this.httpOptions)
      .pipe(
        tap(_ => console.log("User logged in")),
        catchError(this.handleError<User>('login'))
    );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
  }

  registration(newUser: NewUser): Observable<NewUser> {
    return this.http.post<NewUser>(this.url.concat("/v1/authentication/registration/client"), newUser, this.httpOptions)
      .pipe(
        tap(_ => console.log("User registered succesfully", newUser.name, 
                                                            newUser.surname, 
                                                            newUser.email, 
                                                            newUser.phone, 
                                                            newUser.password)),
        catchError(this.handleError<NewUser>('login'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
