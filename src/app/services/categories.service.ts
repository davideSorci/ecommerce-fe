import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Categories } from '../interfaces/categories';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  private url = 'http://13.53.46.222';
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS' })
  };

  constructor(private http: HttpClient) { }

  getAllCategories(): Observable<Categories[]> {
    return this.http.get<Categories[]>(this.url.concat("/v1/category/getAll")).pipe(
      catchError(this.handleError<Categories[]>('getAllCategories'))
    );
  }

  updateCategoryRating(category: Categories): Observable<Categories> {
    return this.http.patch<Categories>(this.url.concat("/v1/category/updateRating"), category, this.httpOptions).pipe(
      tap(_=> console.log("categoria " + category.name + " aggiornata")),
      catchError(this.handleError<Categories>('updateCategoryRating'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}