import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError} from 'rxjs/operators';
import { Products } from '../interfaces/products';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private url = 'http://13.53.46.222';
  //private url = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  /*Tell to backend to change to discounted products API in order not to fetch every single product*/
  getPublishedProducts(): Observable<Products[]> {
    return this.http.get<Products[]>(this.url.concat("/v1/advertisement/published")).pipe(
      catchError(this.handleError<Products[]>('getPublishedProducts'))
    );  
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
