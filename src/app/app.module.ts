import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { AdvSliderComponent } from './pages/home/adv-slider/adv-slider.component';
import { TopCategoriesCarouselComponent } from './pages/home/top-categories-carousel/top-categories-carousel.component';
import { RecommendedComponent } from './pages/home/recommended/recommended.component';
import { SearchComponent } from './pages/search/search.component';
import { NavOptionsFilterComponent } from './components/nav-options-filter/nav-options-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegistrationComponent,
    HeaderComponent,
    NavigationComponent,
    AdvSliderComponent,
    TopCategoriesCarouselComponent,
    RecommendedComponent,
    SearchComponent,
    NavOptionsFilterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
